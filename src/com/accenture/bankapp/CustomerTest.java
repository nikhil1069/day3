package com.accenture.bankapp;

public class CustomerTest {

	public static void main(String[] args) {
		
		
		Customer c1 = new RegCustomer(01, "Nikhil Raj", 100001, 10000, 3.5F);
		System.out.println("Name = "+c1.getCustName());
		System.out.println("Acount no = "+c1.getAccNo());
		System.out.println("Daily Withdrawal limit = "+c1.withdrawLimit());
		
		
		
		Customer c2 = new PremiumCustomer(02, "Rohan Kumar", 100002, 250000, 6.5F);
		System.out.println("Name ="+c2.getCustName());
		System.out.println("Acount no ="+c2.getAccNo());
		System.out.println("Daily Withdrawal limit = "+c2.withdrawLimit());
		

		CustFacilities f =new CustFacilitiesImpl(); 
		
		f.balance();
		
	}
}