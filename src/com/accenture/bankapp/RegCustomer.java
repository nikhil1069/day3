package com.accenture.bankapp;

public class RegCustomer extends Customer {

	private int withdrawAmt;
	private float interestRate;
	
	public int getWithdrawAmt() {
		return withdrawAmt;
	}

	public void setWithdrawAmt(int withdrawAmt) {
		this.withdrawAmt = withdrawAmt;
	}

	public float getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(float interestRate) {
		this.interestRate = interestRate;
	}
	


	public RegCustomer(int custID, String custName, int accNo,int withdrawAmt, float interestRate) {
		super(custID, custName, accNo);
		this.withdrawAmt = withdrawAmt;
		this.interestRate = interestRate;
	}
	


	@Override
	public String toString() {
		return "RegularCustomer [withdrawAmt=" + withdrawAmt + ", interestRate=" + interestRate + ", getCustID()="
				+ getCustID() + ", getCustName()=" + getCustName() + ", getAccNo()=" + getAccNo() + ", toString()="
				+ super.toString() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + "]";
	}

	@Override
	public int withdrawLimit() {
		// TODO Auto-generated method stub
		return withdrawAmt;
	}

	@Override
	public float intRate() {
		// TODO Auto-generated method stub
		return interestRate;
	}

}