package com.accenture.bankapp;

public class PremiumCustomer extends Customer {
	private int pWithdrawAmt;
	private float pInterestRate;
	

	public int getpWithdrawAmt() {
		return pWithdrawAmt;
	}

	public void setpWithdrawAmt(int pWithdrawAmt) {
		this.pWithdrawAmt = pWithdrawAmt;
	}

	public float getpInterestRate() {
		return pInterestRate;
	}

	public void setpInterestRate(float pInterestRate) {
		this.pInterestRate = pInterestRate;
	}
	

	

	public PremiumCustomer(int custID, String custName, int accNo,int pWithdrawAmt, float pInterestRate) {
		super(custID, custName, accNo);
		this.pWithdrawAmt = pWithdrawAmt;
		this.pInterestRate = pInterestRate;
	}


	@Override
	public String toString() {
		return "pCustomer [pWithdrawAmt=" + pWithdrawAmt + ", pInterestRate="
				+ pInterestRate + ", getCustID()=" + getCustID() + ", getCustName()=" + getCustName()
				+ ", getAccNo()=" + getAccNo() + ", toString()=" + super.toString() + ", getClass()=" + getClass()
				+ ", hashCode()=" + hashCode() + "]";
	}

	@Override
	public int withdrawLimit() {
		return pWithdrawAmt;
	}

	@Override
	public float intRate() {
		return pInterestRate;
	}
}
