package com.accenture.bankapp;

public abstract class Customer {

	private int custID;
	private String custName;
	private int accNo;
	
	public int getCustID() {
		return custID;
	}
	public void setCustID(int custID) {
		this.custID = custID;
	}
	public String getCustName() {
		return custName;
	}
	public void setCustName(String custName) {
		this.custName = custName;
	}
	public int getAccNo() {
		return accNo;
	}
	public void setAccNo(int accNo) {
		this.accNo = accNo;
	}
	
	public Customer(int custID, String custName, int accNo) {
		super();
		this.custID = custID;
		this.custName = custName;
		this.accNo = accNo;
	}
	@Override
	public String toString() {
		return "Customer [custID=" + custID + ", custName=" + custName + ", accNo=" + accNo + "]";
	}
	public abstract int withdrawLimit();
	public abstract float intRate();
	
	
	
}
